import React from "react";
import styled from "styled-components";
import Month from "components/organisms/Calendar/Month";

const Container = styled.div`
  padding: 2rem;
`;

const Calendar = () => {
    return (
        <Container>
            <Month />
        </Container>
    )
};

export default Calendar;