import React from "react";
import styled from "styled-components";
import CardList from "components/organisms/Habits/List/CardList";

const Container = styled.div`
  padding: 5rem;
`;

const Habits = () => {
    return (
        <Container>
            <CardList/>
        </Container>
    )
};

export default Habits;