import React from "react";
import styled from "styled-components";
import { Switch, Route } from "react-router-dom";
import Sidebar from "components/organisms/Habits/Sidebar/Sidebar";
import {routes} from "routes";
import Habits from "views/Dashboard/Habits";
import Calendar from "views/Dashboard/Calendar";

const Container = styled.div`
  display: grid;
  grid-template-columns: 25rem 1fr;
  background: #121212;
  min-height: 100vh;
`;

const Dashboard = () => {
    return (
        <Container>
            <Sidebar/>
            <Switch>
                <Route path={routes.habits} component={Habits} />
                <Route exact path={routes.calendar} component={Calendar} />
            </Switch>
        </Container>
    )
};

export default Dashboard;

//syntax: '#555', ui: '#ddd', bg: '#eee'