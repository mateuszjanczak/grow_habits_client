export const routes = {
    homepage: '/',
    habits: '/habits',
    calendar: '/calendar'
};