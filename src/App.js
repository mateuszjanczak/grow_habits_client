import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import styled from "styled-components";
import { routes } from "routes";
import GlobalStyle from "theme";
import Dashboard from "views/Dashboard/Dashboard";

const Box = styled.div`

`;

const App = () => {
  return (
      <BrowserRouter>
        <GlobalStyle />
        <Box>
            <Switch>
                <Route path={routes.homepage} component={Dashboard} />
            </Switch>
        </Box>
      </BrowserRouter>
  )
};

export default App;
