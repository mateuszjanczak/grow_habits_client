import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  background: #332940;
`;

const Details = styled.div`
  padding: 2rem;
`;

const Heading = styled.h2`
  cursor: pointer;
`;

const Status = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
`;

const Progressbar = styled.div`
  height: 1rem;
  border-top: 1px solid black;
`;

const Progress = styled.div`
  width: 21%;
  background: #eee;
  height: calc(1rem - 1px);
`;

const Dates = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
`;

const CardBasicElement = () => {
    return (
        <Wrapper>
            <Details>
                <Heading>Stop Cola</Heading>
                <Status>
                    <div>
                        <p>Upcoming</p>
                        <p>17:00</p>
                    </div>
                    <div>
                        <p>Progress</p>
                        <p>14 / 66</p>
                    </div>
                </Status>
                <Dates>
                    <div>
                        <p>Start date</p>
                        <p>15.02.2020</p>
                    </div>
                    <div>
                        <p>End date</p>
                        <p>20.04.2020</p>
                    </div>
                </Dates>
            </Details>

            <Progressbar>
                <Progress/>
            </Progressbar>
        </Wrapper>
    );
};

export default CardBasicElement;