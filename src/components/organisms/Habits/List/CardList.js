import React from "react";
import styled from "styled-components";
import { default as Element } from "components/organisms/Habits/List/CardBasicElement";

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-gap: 5rem;
`;

const CardList = () => {

    return (
        <Wrapper>
            <Element/>
            <Element/>
            <Element/>
        </Wrapper>
    );
};

export default CardList;