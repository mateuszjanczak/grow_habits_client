import React from "react";
import styled from "styled-components";
import dayjs from "dayjs";

const Wrapper = styled.div`
  background: #332940;
  padding: 5rem;
  display: grid;
  grid-row-gap: 2rem;
  height: 100%;
`;

const Title = styled.h1`
  margin: 0;
  padding: 0;
  text-align: center;
`;

const Table = styled.div`
  
`;

const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(7, 1fr);
  border-bottom: 1px solid black;
  &:first-child {
    border-top: 1px solid black;
  }
`;

const Item = styled.div`
  display: grid;
  justify-content: center;
  align-content: center;
  height: 10rem;
  border-right: 1px solid black;
  color: ${props => props.gray ? "gray" : "white"};
  font-weight: bold;
  
  &:first-child {
    border-left: 1px solid black;
  }
`;

const Action = styled.div`

`;

class Month extends React.Component {

    state = {
        date: '',
        month: '',
        year: ''
    };

    componentDidMount() {
        const date = dayjs();

        this.setState({
            ...this.state,
            date
        }, this.updateDate);
    }

    updateDate = () => {
        const { date } = this.state;

        const month = date.format('MMMM');
        const year = date.year();

        this.setState({
            date,
            month,
            year
        })
    };

    onClickPrevious = () => {
        let { date } = this.state;
        date = date.add(-1, 'month');

        this.setState({
            ...this.state,
            date
        }, this.updateDate);
    };

    onClickNext = () => {
        let { date } = this.state;
        date = date.add(1, 'month');

        this.setState({
            ...this.state,
            date
        }, this.updateDate);
    };

    renderCalendar() {
        const { date } = this.state;

        const firstDayInMonthDate = date.startOf('month');
        const lastDayInMonthDate = date.endOf('month');

        const firstDayInMonthAsDayOfTheWeek = parseInt(firstDayInMonthDate.format('d'));
        const lastDayInMonthAsDayOfTheWeek = parseInt(lastDayInMonthDate.format('d'));
        const numberOfDays = parseInt(lastDayInMonthDate.format('D'));

        const offsetPrev = firstDayInMonthAsDayOfTheWeek === 0 ?  7 : firstDayInMonthAsDayOfTheWeek;
        const previousMonthDaysCount = offsetPrev - 1;
        const offsetNext = lastDayInMonthAsDayOfTheWeek === 0 ?  7 : lastDayInMonthAsDayOfTheWeek;
        const nextMonthDaysCount = 7 - offsetNext;

        let iterableDate = firstDayInMonthDate.subtract(previousMonthDaysCount, 'days');
        const limit = numberOfDays + previousMonthDaysCount + nextMonthDaysCount;

        const rows = [];
        let i = 0;
        while(i < limit){
            const cols = [];
            for(let j = 0; j < 7; j++){
                cols.push(
                    <Item key={i++}>
                        {iterableDate.format('D')}
                    </Item>
                );
                iterableDate = iterableDate.add(1, 'day');
            }

            rows.push(
                <Row key={i++}>
                    {cols}
                </Row>
            );

        }

        return (
            <>
            {rows}
            </>
        )
    }

    render() {

        return (
            <Wrapper>
                <Title>{this.state.month}</Title>
                <Table>
                    <Row>
                        <Item>Monday</Item>
                        <Item>Tuesday</Item>
                        <Item>Wednesday</Item>
                        <Item>Thursday</Item>
                        <Item>Friday</Item>
                        <Item>Saturday</Item>
                        <Item>Sunday</Item>
                    </Row>
                    {this.state.date && this.renderCalendar()}
                </Table>
                <Action>
                    <button onClick={this.onClickPrevious}>Previous</button>
                    <button onClick={this.onClickNext}>Next</button>
                </Action>
            </Wrapper>
        )
    }

}

export default Month;